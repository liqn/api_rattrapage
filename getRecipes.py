from flask import Flask
from flask import jsonify
from flask import Flask, render_template, request
from flask.ext.mysql import MySQL
from flask import Response
import json
import sys
from pprint import pprint
from datetime import datetime, tzinfo, timedelta
from CodeError import *
import re
import uuid
import logging

app = Flask(__name__)

mysql = MySQL()

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'root'
app.config['MYSQL_DATABASE_DB'] = 'api'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

cnx = mysql.connect()

isGenerate = False
file_handler = logging.FileHandler('app.log')
app.logger.addHandler(file_handler)
app.logger.setLevel(logging.INFO)

@app.errorhandler(404)
def pageNotFound(error):
	code = 404
	req_resp = {}
	req_resp['code'] = code
	req_resp['message'] = "Not Found"
	res = jsonify(req_resp)
	res.status_code = code
	return res, code

def execute_query(query, args):
	cur = cnx.cursor()
	cur.execute(query, args)
	res = cur.fetchall()
	cur.close()
	return res

def auth_check(args):
	cur = cnx.cursor()
	query = "SELECT `users__user`.`password` FROM `users__user` WHERE `users__user`.`password` = %s"
	cur.execute(query, args.get('Authorization'))
	res = cur.fetchall()
	cur.close()
	if (len(res) > 0):
		return True
	else:
		return False

def check_slug(args):
	cur = cnx.cursor()
	query = "SELECT slug FROM recipes__recipe  WHERE slug = %s"
	slug = execute_query(query, args.get("slug").strip())
	cur.close()
	if (len(slug) > 0):
		return True
	else:
		return False

def isNotBlank(myString):
    return bool(myString and myString.strip())

def clear_list(lst):
	l = []
	for x in lst:
		if (isNotBlank(x)):
			l.append(x);
	return l

def post_result(user_data, req_data, step):
	req_resp = {
			'code' : 201,
			'message' : "Created",
			"datas" : {
					'id' : req_data.get('id'),
					'name' : req_data.get('name'),
					'user' : {
							'username' : user_data.get('username'),
							'last_login' : user_data.get('last_login'),
							'id' : user_data.get('id')
					},
					'slug' : req_data.get('slug'),
					'step' : clear_list(step)
				}
		}
	return req_resp

def add_new_reciepes(slug, name, pwd, steps):
	cur = cnx.cursor()
	f_steps = get_steps(steps)
	app.logger.info("step =" + str(steps))
	app.logger.info(f_steps)
	query_user = "SELECT id, username, last_login FROM users__user  WHERE password = %s"
	query_reci = "SELECT id, name, slug FROM recipes__recipe  WHERE slug = %s"
	query_insert = "INSERT INTO recipes__recipe VALUES (default, %s, %s, %s, %s)"
	cur.execute(query_user, pwd)
	columns = cur.description
	user_info =  [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
	cur.close()
	try:
		cur = cnx.cursor()
		app.logger.info("error step 2")
		cur.execute(query_insert, (user_info[0].get('id'), name, slug, str(steps)))
		cnx.commit()
		cur.close()
	except Exception as e:
		print(e)
		cnx.rollback()
		cur.close()
		app.logger.info("execute request")
		return unknown_ext(400, "Bad Request")
	cur = cnx.cursor()
	cur.execute(query_reci, slug)
	columns = cur.description
	rec_info =  [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
	req_data = post_result(user_info[0], rec_info[0], f_steps)
	res = jsonify(req_data)
	res.status_code = 201
	cur.close()
	return res, 201

def isStep(string):
	if (string.find("step[") >= 0 and string[len(string) - 1] == ']'):
		return True
	else:
		return False

def get_steps(list_):
	l = []
	for x, y in list_.items():
		if isStep(x) and isNotBlank(y):
			l.append(y)
	app.logger.info(l)
	return l

def isStepInForn(list_):
	for x, y in list_.items():
		if isStep(x):
			return True
	return False

	
@app.route('/api/recipes.<json>', methods = ['GET', 'POST'])
def getRecipes(json):
	if (str(json) == "json" and request.method == "GET"):
		cur = cnx.cursor()
		query = "SELECT id, slug, name FROM recipes__recipe"
		cur.execute(query)
		columns = cur.description
		result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
		
		datas = {
			'code' : 200,
			'message' : "success",
			'datas' : result
		}
		isGenerate = True
		return jsonify(datas)
	if (str(json) == "json" and request.method == "POST"):
		app.logger.info('body------> ' + str(request.data) + ' \nHeaders:-----> ' + str(request.headers) + '\n args:-----> ' + str(request.args) + '\n form:---->' + str(request.form))
		app.logger.info(request.headers.get('Content-Type'))
		f_steps = get_steps(request.form)
		if ("Authorization" not in request.headers or not auth_check(request.headers)):
			return ma_page_401(401)
		if ('application/x-www-form-urlencoded' != request.headers.get('Content-Type')):
			app.logger.info("error content type")
			return ma_page_400(400)
		if (not request.form.get("name") or len(request.form.get("name")) <= 0):
			app.logger.info("error name")
			return ma_page_400(400)
		if (len(f_steps) <= 0):
			app.logger.info("error step 2")
			return ma_page_400(400)
		if (request.form.get("slug")):
			if (check_slug(request.form)):
				app.logger.info("error check slug")
				return ma_page_400(400)
			else:
				slug = request.form.get("slug")
		else:
			slug = str(uuid.uuid1())
		return add_new_reciepes(slug, request.form.get("name"), str(request.headers.get('Authorization')), request.form)
	return ma_page_400(400)

@app.route('/api/recipes/<Recip>.<json>', methods = ['GET'])
def getrecipesCateg(Recip, json):
	#Rexeg = "[^:](\w+)"
	if (str(json) == "json"):
		cur = cnx.cursor()
		queryRecip = "SELECT DISTINCT `recipes__recipe`.`id`, `recipes__recipe`.`user_id`, `recipes__recipe`.`slug`, `recipes__recipe`.`name`\
					FROM `recipes__recipe` \
					WHERE `recipes__recipe`.`slug` = %s "
		queryUser = "SELECT DISTINCT `recipes__recipe`.`user_id`, `users__user`.`username`, `users__user`.`last_login`\
					FROM `recipes__recipe`\
					LEFT JOIN `users__user` ON  `recipes__recipe`.`user_id` = `users__user`.`id` \
					WHERE `recipes__recipe`.`slug` = %s " 
		listRecipCateg = cur.execute(queryRecip, Recip)
		
		if (listRecipCateg == 0):
			code = 404
			return ma_page_404(code)
		else:
			columns = cur.description
			reqCateg = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
			reqRecipUser = cur.execute(queryUser, Recip)
			columns = cur.description
			reqRecipUser = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
			last_login = transformerDate(reqRecipUser[0].get('last_login'))
			isGenerate = True
			return getrecipesResCateg(reqCateg, reqRecipUser)
	else:
		code = 400
		return ma_page_400(code)

		
class TZ(tzinfo):
	def utcoffset(self, dt):
		return timedelta(minutes = 60)

def transformerDate(date):
	if (date == None):
		return date;
	else:
		newDate = datetime(date.year, date.month, date.day, date.hour, date.minute, date.second, tzinfo = TZ()).isoformat()
	return newDate

def getrecipesResCateg(reqCateg,reqRecipUser):
	res = {
		'code' : 200,
		'message' : "success",
		'datas' :{ 
			'user' : reqRecipUser,
			'id': reqCateg[0].get('id'),
			'slug': reqCateg[0].get('slug'),
			'name': reqCateg[0].get('name'),	
		}
	}
	return jsonify(res)

def find_between(s, start, end):
	return (s.split(start))[1].split(end)[0]
	
@app.route('/api/recipes/<Recip>/steps.<json>', methods = ['GET'])
def getrecipesStep(Recip, json):
	if (str(json) == "json"):
		cur = cnx.cursor()
		queryStep = "SELECT DISTINCT `recipes__recipe`.`step`\
					FROM `recipes__recipe`\
					LEFT JOIN `users__user` ON  `recipes__recipe`.`user_id` = `users__user`.`id` \
					WHERE `recipes__recipe`.`slug` = %s " 
		listRecipStep = cur.execute(queryStep, Recip)
		if (listRecipStep == 0):
			code = 404
			return ma_page_404(code)
		else:
			reqRecipStep = cur.execute(queryStep, Recip)
			StepData = []
			for row in cur.fetchall():
				StepData.extend(row)
			pattern = re.compile('.+?"(.+?)"')
			newStepData = str(StepData)
			datas = pattern.findall(newStepData)  
			datas = {
				'code' : 200,
				'message' : "success",
				'datas' : datas
			}
			isGenerate = True
			return jsonify(datas)
	else:
		code = 400
		return ma_page_400(code)
	
if __name__ == "__main__":
	app.config['JSON_SORT_KEYS'] = False
	app.run('0.0.0.0', port=int(sys.argv[1]), debug=True)