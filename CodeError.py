from flask import Flask
from flask import jsonify
from flask import Flask, render_template, request
from flask.ext.mysql import MySQL
from flask import Response
import json
import sys
from pprint import pprint
from datetime import datetime, tzinfo, timedelta

app = Flask(__name__)

@app.errorhandler(400)
def ma_page_400(code):
	data = []
	datas = {
			'code' : code,
			'message' : "Bad Request",
			'datas' : data
		}	
	error = jsonify(datas)
	error.status_code = 400
	return error, code

@app.errorhandler(404)
def ma_page_404(self):
	code = 404
	datas = {
			'code' : code,
			'message' : "Not Found"
		}	
	error = jsonify(datas)
	error.status_code = code
	return error, code

@app.errorhandler(403)
def ma_page_403(self):
	code = 403
	datas = {
			'code' : code,
			'message' : "Forbidden"
		}	
	error = jsonify(datas)
	error.status_code = code
	return error, code

@app.errorhandler(401)
def ma_page_401(self):
	code = 401
	datas = {
			'code' : code,
			'message' : "Unauthorized"
		}	
	error = jsonify(datas)
	error.status_code = code
	return error, code